
import './App.css';
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
      <>
        <div className="Container">
        <LocationForm />
        <ConferenceForm />
        {/* <AttendeesList attendees={props.attendees} /> */}
        </div>
      </>
    );
  }
export default App;
