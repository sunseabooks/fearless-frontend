import React from 'react'

class LocationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name:[],
            room_count:[],
            city:[],
            state:[],
            states:[]
        }
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleNameChange(event) {
        const value = event.target.value
        const name = event.target.name
        this.setState({[name]: value})
    }
    async handleSubmit(event) {
        event.preventDefault()
        const {states, ...data} = this.state
        console.log(data)
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);
            Object.keys(data).map( item => {
                return (
                    this.setState({[item]: []}),
                    document.querySelector(`#${item}`).value=[]
                    )
                }
            )
        }

    }
    async componentDidMount() {
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();

          this.setState({states: data})

        }
      }
    render() {
        return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleNameChange} required name="state" id="state" className="form-select">
                    <option value="">Choose a state</option>

                    {this.state.states.map(item => {
                        return (
                        <option key={item.abbreviation} value={item.abbreviation}>
                            {item.name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        )
    }
}

export default LocationForm
