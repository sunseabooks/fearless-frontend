import React from 'react'

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name:[],
            starts:[],
            ends:[],
            description:[],
            max_presentations:[],
            max_attendees:[],
            location:[],
            locations:[],
        }
        this.handleChangeInput = this.handleChangeInput.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleChangeInput(event) {
        const name = event.target.name
        const value = event.target.value
        this.setState({[name]:value})
    }
    async handleSubmit(event) {
        event.preventDefault()
        const {locations, ...data} = this.state
        data.location = data.location.match(/[0-9]/g).join("")
        const url= "http://localhost:8000/api/conferences/"
        console.log(data)
        const fetchParams = {
            method:"post",
            body:JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchParams)
        if (response.ok) {
            const newConference = await response.json()
            console.log(newConference)
            Object.keys(data).map(item=>{
                return (
                    this.setState({[item]:[]}),
                    document.querySelector(`#${item}`).value=[]
                    )
                }
            )
        }
    }
    async componentDidMount() {
        const url="http://localhost:8000/api/locations/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            this.setState({locations:data.locations})
        }
    }
    render() {
        return (
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeInput} type="text" name="name" id="name" className="form-control" placeholder="" required/>
                        <label htmlFor="name">Conference Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeInput} type="date" name="starts" id="starts" className="form-control" placeholder="Start Date" required/>
                        <label htmlFor="starts">Start Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeInput} type="date" name="ends" id="ends" className="form-control" placeholder="End Date" required/>
                        <label htmlFor="ends">End Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeInput} type="text" name="description" id="description" className="form-control" placeholder="...description" required/>
                        <label htmlFor="description">Description</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeInput} type="number" name="max_presentations" id="max_presentations" className="form-control" placeholder="" required/>
                        <label htmlFor="max_presentations">Maxnumber of presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeInput} type="number" name="max_attendees" id="max_attendees" className="form-control" placeholder="" required/>
                        <label htmlFor="max_attendees">Max number of attendees</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleChangeInput} required id="location" name="location" className="form-select">
                          <option value="">Choose a conference location</option>
                          { this.state.locations.map(item=>{
                            return (<option key={item.href} value={item.href}>{item.name}</option>)
                          })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}
export default ConferenceForm
