
const url_login = "http://localhost:8000/login/"

const parseFetch= async (url, params={}) => {
    const response = await fetch(url, params)
    if (response.ok) {
        data = await response.json()
        return data
    } else {console.error("bad response")}
}

const formTag = document.querySelector(`#login-form`)

window.addEventListener(`DOMContentLoaded`, async ()=> {
    formTag.addEventListener(`submit`, async (event)=> {
        event.preventDefault()
        const formData = new FormData(formTag)

        const fetchParams = {
            method: "POST",
            body: formData,
            credentials: 'include',
        }
        console.log(fetchParams)
        const response = await fetch(url_login, fetchParams);
        if (response.ok) {
          window.location.href = '/';
        } else {
          console.error(response);
        }


    })
})
