const url_locations = `http://localhost:8000/api/locations/`
const url_conferences = `http://localhost:8000/api/conferences/`

const fetchParse = async (url, params={}) => {
    const response = await fetch(url, params)
    const data = await response.json()
    return data
}


window.addEventListener(`DOMContentLoaded`, async ()=> {
    const formSelect = document.querySelector(`select.form-select`)
    const locationList = await fetchParse(url_locations)

    for (let location of locationList.locations) {
        const {name, href} = location
        const locationOption = document.createElement(`option`)
        formSelect.appendChild(locationOption)
        locationOption.innerHTML =`${name}`
        locationOption.value = href.match(/[0-9]/g)

    }

    const formTag = document.querySelector(`#create-conference-form`)
    formTag.addEventListener(`submit`, async (event) => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const data = Object.fromEntries(formData)
        console.log(data)
        // data[`location`] = data.location.match(/[0-9]/g).toString()
        console.log(data)
        const json = JSON.stringify(data)

        const fetchParams = {
            method: "POST",
            body: json,
            headers : {
                'Content-Type': 'application/json',
            },
        }
        const newConference = await fetchParse(url_conferences, fetchParams)
        formTag.reset()
        console.log(newConference)

    })


})
