const url_conferences = "http://localhost:8000/api/conferences/"


const fetchParse = async (url, params={})=> {
    const response = await fetch(url, params)
    if (response.ok) {
        const data = await response.json()
        return data
        } else {return console.error('bad response')}
}
const formTag = document.querySelector(`#create-attendee-form`)
const loadSpinner = document.querySelector(`div.spinner-grow`)
const formSelect = document.querySelector(`select.form-select`)
const successAlert = document.querySelector(`div.alert-success`)

window.addEventListener(`DOMContentLoaded`, async ()=> {
    const conferenceList = await fetchParse(url_conferences)
    for (let conference of conferenceList.conferences) {
        const {href, name}=conference
        const conferenceOption = document.createElement(`option`)
        formSelect.appendChild(conferenceOption)
        console.log(name)
        conferenceOption.innerHTML = `${name}`
        conferenceOption.value = href
    }
    loadSpinner.classList.add('d-none')
    formSelect.classList.remove('d-none')

    formTag.addEventListener(`submit`, async (event)=> {
        event.preventDefault()
        const formData = new FormData(formTag)
        console.log(formData)
        const data = Object.fromEntries(formData)
        console.log(data.conference)

        // const re = new RegExp(/^/, `g`)
        // data['conference']= data.conference.match(/)
        const conference_id = data.conference.match(/[0-9]/g).toString()
        console.log(data)
        const json = JSON.stringify(data)

        const fetchParams = {
            method : "POST",
            body : json,
            headers : {
                'Content-type': 'application/json',
            }
        }
        const url_attendees = `http://localhost:8001/api/conferences/${conference_id}/attendees/`
        const newAttendee = await fetchParse(url_attendees, fetchParams)
        formTag.classList.add(`d-none`)
        successAlert.classList.remove(`d-none`)
        formTag.reset()
        console.log(newAttendee)
    })
})
