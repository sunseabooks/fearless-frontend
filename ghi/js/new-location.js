
const url_states = "http://localhost:8000/api/states/"
const url_locations = "http://localhost:8000/api/locations/"

const fetchParse = async (url, params={}) => {
    const response = await fetch(url, params)
    const data = await response.json()
    return data
}

window.addEventListener(`DOMContentLoaded`, async ()=> {

    const formSelect= document.querySelector(`select.form-select`)
    const stateList = await fetchParse(url_states)
    for (let state of stateList) {
        const {name, abbreviation} = state
        const stateOption = document.createElement("option")
        formSelect.appendChild(stateOption)
        stateOption.innerHTML += `${name}`
        stateOption.value += abbreviation
    }

    const formTag = document.querySelector(`#create-location-form`)
    formTag.addEventListener(`submit`, async (event)=> {
        event.preventDefault()

        const formData  = new FormData(formTag)
        const data = Object.fromEntries(formData)
        const str = data.state
        data[`state`] = str.slice(str.length-2, str.length)
        const json = JSON.stringify(data)

        const fetchParam = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const newLocation = await fetchParse(url_locations, fetchParam)
        formTag.reset()
        console.log(newLocation)

    })

})
