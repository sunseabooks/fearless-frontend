
function createCard(pictureUrl, name, location, description, dates) {
    return `
    <div class="card">
        <img src=${pictureUrl} class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">
            ${location}</h6>
            <p class="card-text">${description}</p>
            <footer class="card-footer">${dates}</footer>
        </div>
    </div>
    `
}
function errorCreate() {
    return [
    `<div class="alert alert-primary" role="alert">
        A simple primary alert—check it out!
    </div>`,
    `<div class="alert alert-secondary" role="alert">
        A simple secondary alert—check it out!
    </div>`]

}





const url_conference_list = `http://localhost:8000/api/conferences/`

window.addEventListener('DOMContentLoaded', async () => {
    const columns = document.querySelectorAll(`.col`)
    console.log(columns)
    try {
        const response = await fetch(url_conference_list);

        if (response.ok) {
            const data = await response.json()

            for (let i=0; i<data.conferences.length;i++) {

                const conference = data.conferences[i]
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)

                if (detailResponse.ok) {

                    const details = await detailResponse.json()
                    const {
                        name,
                        description,
                        starts,
                        ends,
                        location:{picture_url}
                    } = details.conference
                    const locationName = details.conference.location.name
                    console.log(starts)
                    const dates = `
                    Start date: ${starts.substring(0,10)}
                    End date: ${ends.substring(0,10)}
                    `
                    const html = createCard(picture_url, name, locationName, description, dates)
                    const column = columns[i]
                    column.innerHTML += html
                    console.log(name, description, picture_url, dates)
                }
            }

        } else {
            const html = errorCreate()[1]
            const column = columns[0]
            column.innerHTML += html
        }
    } catch (e) {
        console.log("1st")
        const html = errorCreate()[0]
        const column = columns[0]
        column.innerHTML += html

    }
});
